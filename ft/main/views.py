from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from mix.views import lista

# Create your views here.

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect(lista)
    else:
        return render(request, 'login.html')
