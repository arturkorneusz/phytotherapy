from herba.models import Herba
from mix.models import Mix
from rest_framework import serializers

class HerbaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Herba
        fields = ['nazwa', 'opis']

class MixSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mix
        fields = ['nr', 'nazwa']