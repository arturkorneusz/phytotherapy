from django.db import router
from django.urls import path, include
from rest_framework import routers, urlpatterns
from .views import HerbaView, RawHerbaView, MixView

router = routers.DefaultRouter()
router.register(r'herba', HerbaView)
router.register(r'rawherba', RawHerbaView)
router.register(r'mix', MixView)

urlpatterns = [
    path('', include(router.urls)),
    
]