from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from herba.models import Herba
from mix.models import Mix
from .serializers import HerbaSerializer, MixSerializer

# Create your views here.

#only names - solution from https://stackoverflow.com/questions/46125398/how-to-display-only-values-in-django-serializers
class RawHerbaView(viewsets.ModelViewSet):
    queryset = Herba.objects.values('nazwa')
    serializer_class = HerbaSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        return Response(queryset.values_list('nazwa', flat=True))

class HerbaView(viewsets.ModelViewSet):
    queryset = Herba.objects.all()
    serializer_class = HerbaSerializer

class MixView(viewsets.ModelViewSet):
    queryset = Mix.objects.all()
    serializer_class = MixSerializer