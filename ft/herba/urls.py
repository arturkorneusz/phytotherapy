from django.urls import path
from herba.views import test_response, lista, nowy, zmien, usun

urlpatterns = [
    path('test/', test_response),
    path('', lista, name='ziola'),
    path('nowy/', nowy, name='n'),
    path('zmien/<int:id>/', zmien, name='e'),
    path('usun/<int:id>/', usun, name='u')
]