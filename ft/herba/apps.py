from django.apps import AppConfig


class HerbaConfig(AppConfig):
    name = 'herba'

    def ready(self):
        import herba.signals
