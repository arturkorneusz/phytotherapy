from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from .models import Herba

@receiver(post_save, sender=Herba)
def herba_after_save(sender, instance, **kwargs):
    print('Dodano nowe zioło:', instance.nazwa)

#post_save.connect(herba_after_save, sender=Herba) #zamiast dekoratore reciver

@receiver(post_delete, sender=Herba)
def herba_after_delete(sender, instance, **kwargs):
    print('Usunięto zioło:', instance.nazwa)