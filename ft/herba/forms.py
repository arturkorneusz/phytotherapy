from django.forms import ModelForm
from herba.models import Herba

class HerbaForm(ModelForm):
    class Meta:
        model = Herba
        fields = ['nazwa','opis']