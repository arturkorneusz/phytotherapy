from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from herba.models import Herba
from herba.forms import HerbaForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
#pylint: disable=no-member

def test_response(request):
    #return HttpResponse("test test test")
    return render(request, 'test.html')

def lista(request):
    wszystkie = Herba.objects.all().order_by('nazwa')
    return render(request, 'ziola_lista.html', {'lista': wszystkie})

@login_required
def nowy(request):
    form = HerbaForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request, "Wpis został dodany")
        return redirect(lista)
    return render(request, 'ziola_form.html', {'form': form})

@login_required
def zmien(request, id):
    ziolo = get_object_or_404(Herba, pk=id)
    form = HerbaForm(request.POST or None, instance=ziolo)
    if form.is_valid():
        form.save()
        return redirect(lista)
    return render(request, 'ziola_form.html', {'form': form})

@login_required
def usun(request, id):
    ziolo = get_object_or_404(Herba, pk=id)
    if request.method == "GET":
        ziolo.delete()
        messages.success(request, "Wpis został usunięty")
    return redirect(lista)