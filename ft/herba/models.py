from django.db import models

# Create your models here.

class Herba(models.Model):
    nazwa = models.CharField(max_length=64, null=False, unique=True)
    opis = models.TextField(null=True, blank=True, default="")

    class Meta:
        verbose_name = 'Zioło'
        verbose_name_plural = 'Zioła'

    def __str__(self):
        return self.nazwa

