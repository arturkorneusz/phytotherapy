from django.db import models
from herba.models import Herba

# Create your models here.

class Mix(models.Model):
    nr = models.CharField(max_length=4, null=False, unique=True)
    nazwa =  models.CharField(max_length=64, null=False)
    opis = models.TextField(null=True, blank=True, default="")
    skladniki = models.ManyToManyField(Herba, verbose_name="składniki")

    class Meta:
        verbose_name = 'Mieszanka'
        verbose_name_plural = 'Mieszanki'
        
    def __str__(self):
        return self.nr + " (" + self.nazwa + ")"