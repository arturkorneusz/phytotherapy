from django.contrib import admin

# Register your models here.

from mix.models import Mix

admin.site.register(Mix)