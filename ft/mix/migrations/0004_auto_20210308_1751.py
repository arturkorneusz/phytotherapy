# Generated by Django 3.1.4 on 2021-03-08 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mix', '0003_auto_20210226_1618'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mix',
            name='nazwa',
            field=models.CharField(max_length=64),
        ),
    ]
