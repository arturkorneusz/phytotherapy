from django.forms import ModelForm
from mix.models import Mix

class MixForm(ModelForm):
    class Meta:
        model = Mix
        fields = ['nr', 'nazwa', 'opis', 'skladniki']