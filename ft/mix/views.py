from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from mix.models import Mix
from mix.forms import MixForm

# Create your views here.
#pylint: disable=no-member

def test_response(request):
    return HttpResponse("test test test")

def lista(request):
    search_term = ''
    #wszystkie = Mix.objects.all().order_by('nr')
    wszystkie = Mix.objects.all()

    if 'search' in request.GET:
        search_term = request.GET['search']
        wszystkie = Mix.objects.filter(skladniki__nazwa__startswith=search_term)     
    
    return render(request, 'mieszanki_lista.html', {'lista': wszystkie, 'search': search_term})

def nowy(request):
    form = MixForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(lista)
    return render(request, 'mieszanki_form.html', {'form': form})

def zmien(request, id):
    mieszanka = get_object_or_404(Mix, pk=id)
    form = MixForm(request.POST or None, instance=mieszanka)
    if form.is_valid():
        form.save()
        return redirect(lista)
    return render(request, 'mieszanki_form.html', {'form': form})

def usun(request, id):
    mix = get_object_or_404(Mix, pk=id)
    if request.method == "GET":
        mix.delete()
    return redirect(lista)