from django.urls import path
from mix.views import test_response, lista, nowy, zmien, usun

urlpatterns = [
    path('', lista, name='mieszanki'),
#    path('ziola/', lista, name='ziola'),
    path('nowy/', nowy, name='nowy'),
    path('zmien/<int:id>/', zmien, name='edytuj'),
    path('usun/<int:id>/', usun, name='usun')
]